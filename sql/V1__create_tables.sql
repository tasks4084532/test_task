CREATE TABLE emias.patients
(
    id            int8      NOT NULL GENERATED ALWAYS AS IDENTITY,
    first_name    varchar   NOT NULL,
    last_name     varchar   NOT NULL,
    middle_name   varchar NULL,
    birth_date    date      NOT NULL,
    registered    timestamp NOT NULL DEFAULT now(),
    last_modified timestamp NOT NULL DEFAULT now(),
    CONSTRAINT patients_pk PRIMARY KEY (id)
);
CREATE TABLE emias.doctors
(
    id            int8      NOT NULL GENERATED ALWAYS AS IDENTITY,
    first_name    varchar   NOT NULL,
    last_name     varchar   NOT NULL,
    middle_name   varchar NULL,
    work_start    timestamp NOT NULL DEFAULT now(),
    last_modified timestamp NOT NULL DEFAULT now(),
    CONSTRAINT doctors_pk PRIMARY KEY (id)
);
CREATE TABLE emias.registrations
(
    patient_id           int8 NULL,
    doctor_id            int8 NULL,
    id                   int8      NOT NULL GENERATED ALWAYS AS IDENTITY,
    appointment_time     timestamp NOT NULL,
    created              timestamp NOT NULL DEFAULT now(),
    last_modified        timestamp NOT NULL DEFAULT now(),
    appointment_duration int4      NOT NULL,
    CONSTRAINT registrations_pk PRIMARY KEY (id),
    CONSTRAINT registrations_fk FOREIGN KEY (doctor_id) REFERENCES emias.doctors (id),
    CONSTRAINT registrations_fk_1 FOREIGN KEY (patient_id) REFERENCES emias.patients (id)
);

