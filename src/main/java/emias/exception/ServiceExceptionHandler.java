package emias.exception;

import emias.response.OnlyMessageResponse;
import jakarta.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * Обработчик ошибок.
 */
@Slf4j
@ControllerAdvice
public class ServiceExceptionHandler {

    /**
     * Ошибка отсутсвия параметров в запросе.
     */
    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public OnlyMessageResponse handleValidationException(ConstraintViolationException ex) {
        List<String> invalidParameters = new ArrayList<>();
        for (var error : ex.getConstraintViolations()) {
            invalidParameters.add(error.getPropertyPath() + " = " + error.getInvalidValue().toString());
        }
        log.warn("ValidationResponseException is handled - Response 400");
        return new OnlyMessageResponse("Incorrect request parameters: "
                + String.join(", ", invalidParameters));
    }


    /**
     * Кастомное сообщение об ошибке.
     * Возвращает текст ошибки.
     */
    @ExceptionHandler({OnlyMessageException.class})
    @ResponseBody
    public ResponseEntity<OnlyMessageResponse> handleCustomGetException(OnlyMessageException ex) {
        log.warn("CustomOnlyMessageException is handled - Response "
                + ex.getStatus() + ", message: {} ", ex.getMessage());
        return new ResponseEntity<>(new OnlyMessageResponse(ex.getMessage()), ex.getStatus());
    }

    /**
     * Ошибка работы сервиса.
     */
    @ExceptionHandler({Exception.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public OnlyMessageResponse handleException(Exception ex) {
        log.error("Exception is handled - Response 500, message: " + ex.getMessage());
        return new OnlyMessageResponse("Internal server error");
    }
}