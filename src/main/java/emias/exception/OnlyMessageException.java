package emias.exception;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

/**
 * Текстовая ошибка.
 */
@Getter
@Setter
@AllArgsConstructor
public class OnlyMessageException extends Exception {

    @Schema(description = "Сообщение об ошибке для пользователя")
    private final String message;
    private final HttpStatus status;
}