package emias.repository;

import emias.entity.Doctor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Репозиторий с данными о врачах.
 */
@Repository
public interface DoctorRepo extends CrudRepository<Doctor, String> {

}