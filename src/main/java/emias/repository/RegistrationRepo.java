package emias.repository;

import emias.entity.Registration;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Репозиторий с данными о записях пациентов.
 */
@Repository
public interface RegistrationRepo extends CrudRepository<Registration, String> {

    @Query(value = "SELECT * FROM emias.registrations "
            + "WHERE doctor_id = cast(:doctor_id AS INT) "
            + "AND patient_id IS NULL "
            + "and date_trunc('day', appointment_time) =  make_date("
            + "cast(:year AS INT), cast(:month AS INT), cast(:day  AS INT))",
            nativeQuery = true)
    List<Registration> getEmptyRegistrationsByDoctorId(
            @Param("year") String year,
            @Param("month") String month,
            @Param("day") String day,
            @Param("doctor_id") String doctorId);

    @Query(value = "SELECT appointment_time "
            + "FROM emias.registrations "
            + "WHERE patient_id = cast(:patient_id AS int)",
            nativeQuery = true)
    List<String> getRegistrationsByPatientId(@Param("patient_id") String patientId);

}