package emias.repository;

import emias.entity.Patient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Репозиторий с данными о пациентах.
 */
@Repository
public interface PatientRepo extends CrudRepository<Patient, String> {

}