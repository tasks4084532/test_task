package emias.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.sql.Timestamp;

/**
 * Информация о записях пациентов.
 */
@Data
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Table(name = "registrations", schema = "emias")
public class Registration {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "patient_id")
    private Long patientId;

    @Column(name = "doctor_id")
    private Long doctorId;

    @EqualsAndHashCode.Exclude
    @Column(name = "appointment_time", nullable = false, updatable = false)
    @CreationTimestamp
    private Timestamp appointmentTime;

    @EqualsAndHashCode.Exclude
    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private Timestamp created;

    @EqualsAndHashCode.Exclude
    @Column(name = "last_modified", nullable = false)
    @UpdateTimestamp
    private Timestamp modified;
}