package emias.entity;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.sql.Timestamp;

/**
 * Информация о пациенте.
 */
@Data
@Entity
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Table(name = "patients", schema = "emias")
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "middle_name")
    private String middleName;

    @EqualsAndHashCode.Exclude
    @Column(name = "birth_date", nullable = false, updatable = false)
    @CreationTimestamp
    private Timestamp birthDate;

    @EqualsAndHashCode.Exclude
    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private Timestamp registered;

    @EqualsAndHashCode.Exclude
    @Column(name = "last_modified", nullable = false)
    @UpdateTimestamp
    private Timestamp modified;
}
