package emias.config;


import emias.controller.MainController;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.utils.SpringDocUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Конфигурация сваггера.
 */
@Configuration
public class SwaggerConfig {
    /**
     * Настройки описания сваггера.
     */
    @Bean
    public OpenAPI api() {
        SpringDocUtils.getConfig().addRestControllers(MainController.class);
        return new OpenAPI()
                .info(new Info().title("EMIAS")
                        .description("API для получения и добавления записей ко врачу")
                        .contact(new Contact().email("ankastelin@gmail.com").name("Андрей Кастелин"))
                        .version("v1.0.0"))
                .externalDocs(new ExternalDocumentation()
                        .description("Source code")
                        .url("https://gitlab.com/tasks4084532/test_task"));
    }
}
