package emias.controller;

import emias.entity.Registration;
import emias.exception.OnlyMessageException;
import emias.repository.DoctorRepo;
import emias.repository.PatientRepo;
import emias.repository.RegistrationRepo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.Pattern;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * Контроллер для получения и создания записей.
 */
@RestController
@Slf4j
@Tag(name = "Основной контроллер", description = "Контроллер для получения и создания записей")
@RequestMapping("/api")
@Validated
@RequiredArgsConstructor
public class MainController {
    private final RegistrationRepo registrationRepo;
    private final PatientRepo patientRepo;
    private final DoctorRepo doctorRepo;

    /**
     * Возвращшает все записи пациента по его id.
     *
     * @param patientId - id пациента
     */
    @GetMapping(path = "/patient/appointments")
    @Operation(description = "Получение всех записей пациент по его id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешно"),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос"),
            @ApiResponse(responseCode = "404", description = "Данные не найдены"),
            @ApiResponse(responseCode = "500", description = "Внутрення ошибка сервиса")
    })
    public ResponseEntity<List<String>> getPatientAppointments(
            @Parameter(name = "id", description = "ID пациента", required = true, example = "1")
            @RequestParam(value = "id")
            @Pattern(regexp = "[1-9]\\d*")
            String patientId
    ) throws OnlyMessageException {
        log.trace("request /patient/appointments: id = " + patientId);

        if (patientRepo.findById(patientId).isEmpty()) {
            throw new OnlyMessageException("There is no patient with id " + patientId, HttpStatus.NOT_FOUND);
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(registrationRepo.getRegistrationsByPatientId(patientId));
    }

    /**
     * Получение свободных слотов времени к указанному врачу на указанную дату.
     *
     * @param doctorId - id врача
     */
    @GetMapping(path = "/doctor/appointments/empty")
    @Operation(description = "Получение свободных слотов времени к указанному врачу на указанную дату")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешно"),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос"),
            @ApiResponse(responseCode = "404", description = "Данные не найдены"),
            @ApiResponse(responseCode = "500", description = "Внутрення ошибка сервиса")
    })
    public ResponseEntity<List<Registration>> getDoctorFreeTime(
            @Parameter(name = "id", description = "ID врача", required = true, example = "1")
            @RequestParam(value = "id")
            @Pattern(regexp = "[1-9]\\d*")
            String doctorId,
            @Parameter(name = "date", description = "Дата", example = "")
            @RequestParam(value = "date")
            @Pattern(regexp = "\\d{4}-\\d{2}-\\d{2}|\\d{2}/\\d{2}/\\d{4}|\\d{2}.\\d{2}.\\d{4}")
            String date
    ) throws OnlyMessageException {
        log.trace("request /doctor/appointments/empty: id = " + doctorId, ", date = " + date);

        if (doctorRepo.findById(doctorId).isEmpty()) {
            throw new OnlyMessageException("There is no doctor with id " + doctorId, HttpStatus.NOT_FOUND);
        }

        String[] splitDate = date.split("[/.\\-]");

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(registrationRepo
                        .getEmptyRegistrationsByDoctorId(splitDate[0], splitDate[1], splitDate[2], doctorId));
    }

    /**
     * Запись в свободный слот времени.
     *
     * @param appointmentId - id записи
     * @param patientId     - id пациента
     */
    @PostMapping(path = "/appointment/make")
    @Operation(description = "Получение свободных слотов времени к указанному врачу на указанную дату")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешно"),
            @ApiResponse(responseCode = "400", description = "Некорректный запрос"),
            @ApiResponse(responseCode = "404", description = "Данные не найдены"),
            @ApiResponse(responseCode = "500", description = "Внутрення ошибка сервиса")
    })
    public ResponseEntity<String> makeAppointment(
            @Parameter(name = "appointment_id", description = "ID записи", required = true, example = "1")
            @RequestParam(value = "appointment_id")
            @Pattern(regexp = "[1-9]\\d*")
            String appointmentId,
            @Parameter(name = "patient_id", description = "ID пациента", required = true, example = "1")
            @RequestParam(value = "patient_id")
            @Pattern(regexp = "[1-9]\\d*")
            String patientId
    ) throws OnlyMessageException {

        Optional<Registration> registrationOptional = registrationRepo.findById(appointmentId);
        if (registrationOptional.isEmpty()) {
            throw new OnlyMessageException("There is no appointment with id " + appointmentId, HttpStatus.NOT_FOUND);
        }
        if (patientRepo.findById(patientId).isEmpty()) {
            throw new OnlyMessageException("There is no patient with id " + patientId, HttpStatus.NOT_FOUND);
        }

        Registration registration = registrationOptional.get();
        if (registration.getPatientId() != null) {
            throw new OnlyMessageException("Appointment with id " + appointmentId + " is already busy",
                    HttpStatus.CONFLICT);
        }

        registration.setPatientId(Long.parseLong(patientId));
        registrationRepo.save(registration);
        return ResponseEntity.status(HttpStatus.OK).body("The patient has been successfully enrolled");
    }

}
